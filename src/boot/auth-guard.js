import { boot } from 'quasar/wrappers'
import auth from 'src/auth'
import constants from 'src/constants'
// import auth from 'src/auth'
// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async ({ router }) => {
  router.beforeEach((from, to, next) => {
    if (!auth.currentUser && ![constants.AUTH_URL, '/about'].includes(from.path)) {
      next({ path: '/login' })
    } else {
      next()
    }
  })
})
